Apache CXF 3.1.7 Release Notes

1. Overview

The 3.1.7 version of Apache CXF is a significant new version of CXF 
that provides several new features and enhancements.  

New features include: 

* New Metrics feature for collecting metrics about a CXF services.  
* New Throttling feature for easily throttling CXF services.  
* New Logging feature for more advanced logging than the logging 
  available in cxf-core
* New Metadata service for SAML SSO to allow you to publish SAML SSO 
  metadata for your service provider.
* Enhancements to the code generator to be more "Java7" friendly
* Update to OpenSAML 3.0
* Support for Karaf 4, Jetty 9

Important notes:
CXF 3.1.x no longer supports Java 6.   You must upgrade to Java 7 or later.

Users are encourage to review the migration guide at:
http://cxf.apache.org/docs/31-migration-guide.html
for further information and requirements for upgrading from earlier
versions of CXF.

3.1.7 fixes over 75 JIRA issues reported by users and the community.



2. Installation Prerequisites 

Before installing Apache CXF, make sure the following products,
with the specified versions, are installed on your system:

    * Java 7 Development Kit
    * Apache Maven 3.x to build the samples


3.  Integrating CXF Into Your Application

If you use Maven to build your application, you need merely add
appropriate dependencies. See the pom.xml files in the samples.

If you don't use Maven, you'll need to add one or more jars to your
classpath. The file lib/WHICH_JARS should help you decide which 
jars you need.

4. Building the Samples

Building the samples included in the binary distribution is easy. Change to
the samples directory and follow the build instructions in the README.txt file 
included with each sample.

5. Reporting Problems

If you have any problems or want to send feedback of any kind, please e-mail the
CXF dev list, dev@cxf.apache.org.  You can also file issues in JIRA at:

http://issues.apache.org/jira/browse/CXF

6. Migration notes:

See the migration guide at:
http://cxf.apache.org/docs/31-migration-guide.html
for caveats when upgrading from CXF 2.7.x and 3.0.x
to 3.1.



7. Specific issues, features, and improvements fixed in this version



** Bug
    * [CXF-6454] - Orphaned JMS connections created in endless loop
    * [CXF-6463] - AbstractHTTPDestination.cacheInput() throws NullPointerException if HttpServletRequest returns null for getInputStream()
    * [CXF-6510] - LoggingOutInterceptor: formatLoggingMessage method is not used in every case
    * [CXF-6646] - CXF 3.x WSRM message may not be retrieved from database
    * [CXF-6729] -  Version 1 NewCookie is not compliant with RFC 2109
    * [CXF-6841] - Fix documentation cxf-rt-rs-http-sc => cxf-rt-rs-http-sci
    * [CXF-6842] - Unwrap exception nested with WebApplicationException
    * [CXF-6845] - Some methods in MessageUtils prone to NPE
    * [CXF-6850] - javax.ws.rs.core.Request implementation doesn't match Accept-Encoding: *  for  any encoding value
    * [CXF-6851] - JAXRS 2 Feature not supported on server side?
    * [CXF-6854] - Application subclass can't be injected into an application lass field
    * [CXF-6855] - ElementClass annotation is ignored on JAX-RS service interface when generating the WADL
    * [CXF-6862] - Quoted path field in Cookies appears to be ignored by Chrome and Firefox
    * [CXF-6863] - WS-RM 3.x does not work with attachments upon a network error
    * [CXF-6867] - Envelope and Body element prefixes changed when processing messages without headers
    * [CXF-6868] - empty Authorization header result in server error
    * [CXF-6872] - Remove redundant blueprint.xml in http-hc
    * [CXF-6878] - Protect against other exception during consuming left-over data
    * [CXF-6883] - Crypto caching issues in the WS-Security code
    * [CXF-6884] - Don't include Signature/EncryptedKey Elements if there are no references to be signed/encrypted
    * [CXF-6886] - CXF 3.x WSRM attachments are not retransmitted
    * [CXF-6887] - http-hc: NPE and incorrept assumption that there is only one bus
    * [CXF-6890] - "afirmative" is mispelled in debug output
    * [CXF-6891] - IOUtils.isEmpty() doesn't reinclude byte in stream.
    * [CXF-6900] - invalid signature in case of soap fault
    * [CXF-6901] - UriBuilder may lose resolved query templates
    * [CXF-6906] - UriBuilder ignores a query component if URI contains templates
    * [CXF-6908] - Prefix "SOAP-ENV" for element "SOAP-ENV:Fault" is not bound
    * [CXF-6914] - JweJsonWriterInterceptor adds double quotes for non String unencoded payloads 
    * [CXF-6915] - Jws Compact does not support unencoded non-detached payloads
    * [CXF-6923] - org.omg.CORBA.TIMEOUT is not handled with Jacorb implementation
    * [CXF-6926] - StaticSTSProperties does not allow initialization of crypto from Properties object
    * [CXF-6927] - check if msv is available in Stax2ValidationUtils to avoid the NCDFE when use IBM JDK
    * [CXF-6933] - WadlGenerator doesn't honor multiple Descriptions for same DocTarget
    * [CXF-6939] - can't install cxf-http-async feature
    * [CXF-6943] - Dead lock on Async Response when timeout is set
    * [CXF-6945] - cxf-wadl2java-plugin wadlRoot configuration parameter typo
    * [CXF-6948] - WebClient may cause JMX CounterRepository OOM if a request URI varies a lot  
    * [CXF-6957] - JAX-RS: ExceptionMapper not called for Fault
    * [CXF-6959] - Error loading Aegis Databinding in OSGi
    * [CXF-6961] - Lots of invalid checkstyle errors reported in eclipse neon
    * [CXF-6966] - Using CXF in JDK endorsed dir as JAX-WS impl crashes
    * [CXF-6967] - Content Disposition filename should be case-insensitive
    * [CXF-6970] - HTTP response headers are always set with HttpServletResponse.addHeader
    * [CXF-6972] - JweJsonProducer does not support per-recipient headers


** Improvement
    * [CXF-5193] - Support fixed data type
    * [CXF-6834] - add support for CXF inside Spring Boot
    * [CXF-6837] - Add cache for MessageBodyReader/Writer
    * [CXF-6861] -  Introduce a typed JAXBElement provider 
    * [CXF-6871] - Adjust default User-Agent header format to better comply with HTTP specification
    * [CXF-6875] - Update Apache Mina from 2.0.9 to 2.0.13
    * [CXF-6877] - Have @SchemaValidation working on service endpoint implementation class method
    * [CXF-6903] - add a NameDigestPasswordCallbackHandler for JAASLoginInterceptor
    * [CXF-6910] - don't need setSocketTimeout when create ahc RequestConfig
    * [CXF-6912] - introduce CONNECTION_MAX_IDLE property for AHC
    * [CXF-6918] - Print the XMLInputFactory implementation class when throwing "Cannot create a secure XMLInputFactory"
    * [CXF-6922] - Make JaxRsConfig not interfere with SpringBoot auto configuration process
    * [CXF-6925] - Make per-realm crypto configuration as flexible as the static one
    * [CXF-6935] - Better error message than java.lang.NullPointerException - org.apache.cxf.common.util.Compiler.useJava6Compiler(Compiler.java:187) when running on a JRE instead of JDK
    * [CXF-6936] - Make log-category for ext logging feature configurable
    * [CXF-6947] - Make it possible to use custom LDAP filters when retrieving group information
    * [CXF-6949] - Add support to the ReceivedTokenCalbackHandler to return a transformed token
    * [CXF-6951] - Support using the initiator token's public key for response encryption by recipient
    * [CXF-6953] - Update service list formatters for REST endpoints to optionally link to Swagger
    * [CXF-6960] - Provide an option for Swagger2Feature to detect and serve SwaggerUI resources 


** New Feature
    * [CXF-5091] - Leverage Spring's @Configuration mechanism to simplify the creation and configuration of client proxies for integration testing
    * [CXF-6869] - Consider adding Spring Boot starter
    * [CXF-6879] - JAX-RS Feature and SpringComponentScanServer
    * [CXF-6909] - Create an JCache based OAuthDataProvider
    * [CXF-6973] - Allow to configure http conduits and destinations using features

** Task
    * [CXF-6760] - extract swagger2 feature in its own module
    * [CXF-6853] - Support encoded value in @ApplicationPath
    * [CXF-6858] - Upgrade Xalan bundle to 2.7.2_3
    * [CXF-6895] - Create DOM4JProvider test reading an XML sequence with BOM
    * [CXF-6938] - Setting the providers on a bus causes a leak if this bus is used by per-request clients
    * [CXF-6971] - Update Jettison version to 1.3.8


** Test
    * [CXF-6202] - Create JWS JoseCookBook tests

