package Customer;

import Dao.*;

import java.util.*;

/**
 * Created by Matthew on 10/21/2016.
 */
public class CustomerManager {
    public static CustomerDAO dao = new CustomerDAO();

    public CustomerManager(){

    }//close underloaded Constructor


    public Customer addCustomer(Customer x){
        dao.addCustomer(x);

        return x;
    }//close class

    public Set<Customer> getAllCustomers(){

        return dao.getAllCustomers();
    }//close getAllCustomers()

    public Customer getCustomerById(int id){

        return dao.getCustomerById(id);
    }//close getCustomerById()

    public void updateCustomer(Customer x){
        dao.updateCustomer(x);
    }//close updateCustomer()

    public void deleteCustomer(Customer x){
        dao.deleteCustomer(x);
    }//close deleteCustomer()




}
