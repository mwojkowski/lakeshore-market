package Customer;

import Person.*;
import Dao.*;

import javax.xml.bind.annotation.XmlRootElement;

import java.sql.*;


/**
 * Created by mwojkowski1 on 10/3/2016.
 */

@XmlRootElement
public class Customer{

    private int id;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;
    private String city;
    private String state;
    private String zip;

    public Customer(){

    }//close underloaded constructor

    public Customer(String firstName, String lastName, String address, String phoneNumber, String city, String state, String zip){
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }//close Customer.Customer
    public Customer(int id, String firstName, String lastName, String address, String phoneNumber, String city, String state, String zip){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }//close Customer.Customer

    public int getID(){ return id ; }

    public void setID(int id){
        this.id = id;
    }

    public String getFirstName(){   return firstName;   }

    public String getLastName(){    return lastName;    }

    public String getAddress(){     return address;     }

    public String getPhoneNumber(){ return phoneNumber; }

    public String getCity(){    return city;    }

    public String getState(){   return state;   }

    public String getZip(){ return zip; }

    public void setFirstName(String x){   this.firstName = x;    }

    public void setLastName(String x){  this.lastName = x;  }

    public void setAddress(String x){   this.address = x;   }

    public void setPhoneNumber(String x){   this.phoneNumber = x; }

    public void setCity(String x){  this.city = x;  }

    public void setState(String x){ this.state = x; }

    public void setZip(String x){   this.zip = x;    }








}//close Customer.Customer class
