package Product;
/**
 * Created by mwojkowski1 on 9/28/2016.
 */
public class Product {
    private int id;
    private String name;
    private String description;
    private boolean status;//item may be discontinued or recalled... we can deactivate an item here

    public Product(String name, String description, boolean status){
        this.name = name;
        this.description = description;
        this.status = status;

    }//close overloaded Constructor

    public int getID(){ return id;  }

    public String getName(){    return name;    }

    public String getDescription(){ return description; }

    public boolean getStatus(){ return status;  }

    public void setName(String x){  this.name = x;   }

    public void setDescription(String x){   this.description = x;   }

}//close class
