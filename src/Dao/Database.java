package Dao;
/**
 * Created by mwojkowski1 on 9/28/2016.
 * Driver for Database, including connection string, and database itself
 */
import java.sql.*;

public class Database {
    private String username = "venom";//LakeShoreDriver
    private String password = "Dr@gon45";//ZV9~D?phk#SF2)w
    private String server = "198.12.13.34:3306";
    private String databaseName = "LakeShore";
    private Connection con;
    private PreparedStatement query;

    private String connectionString = "jdbc:mysql://" + server + "/?user=" + username + "&password=" + password;

    public Database() {

    }//close underloaded constructor


    //used to connect to database
    //will return false if we cant establish a connection;
    public Connection Connect() {
        try{
            con = DriverManager.getConnection(connectionString);
        }//close try statement
        catch(Exception e){
            System.out.println(e.getMessage());
        }//close catch statement+
        return con;
    }//close Connect()

    //kills the server connection
    public void close(){
        try{
            con.close();
        }//close try statement
        catch(Exception e){
            System.out.println(e.getMessage());
        }//close catch statement

        con = null;
    }//close Close()


    //used for retrieving information from the database universally for all classes.
        //each class will use this in their own way through their own queries.
        //if the query fails, it will return a null ResultSet, implying an unsuccessful query
    /*public ResultSet select(String x){
        ResultSet result = null;

        try{
            PreparedStatement query = con.prepareStatement("x");

            result = query.executeQuery(x);
        }//close try
        catch(Exception e){
            System.out.println(e.getMessage());
            return result;
        }//close catch statement

        return result;
    }//close select

    //used to run general queries that dont require a result set including:
        //delete, insert, update etc.
    public void query(String x){
        try{
            Class.forName("MySQLDriver");
            Statement query = con.createStatement();
            query.execute(x);
        }//close try statement
        catch(Exception e){
            System.out.println(e.getMessage());
        }//close catch statement

    }//close update()*/







}//close Database class
