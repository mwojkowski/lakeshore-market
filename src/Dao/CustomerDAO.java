package Dao;

import Customer.*;

import java.sql.*;
import java.util.*;


/**
 * Created by Matthew on 10/21/2016.
 */
public class CustomerDAO {

    //******MySQL DATABASE CONNECTION DETAILS*****
    private String server = "jdbc:mysql://162.218.48.24:3306/Lakeshore";
    private String username = "LakeShore";
    private String password = "284028";

    Connection connection;

    private static Set<Customer> customers = new HashSet<Customer>();

    public CustomerDAO(){

    }//close CustomerDAO underloaded constructor



    public Set<Customer> getAllCustomers(){
        ResultSet temp;

        try{
            connection = DriverManager.getConnection(server, username, password);

            String prequery = "Select * FROM Customers";
            PreparedStatement query = connection.prepareStatement(prequery);

            temp = query.executeQuery();

            while(temp.next()){
                customers.add(new Customer(temp.getInt("id"), temp.getString("FirstName"), temp.getString("LastName"), temp.getString("Address"),
                        temp.getString("PhoneNumber"), temp.getString("City"), temp.getString("State"), temp.getString("Zip")));
            }//close while statement


            connection.close();


        }//close try statement
        catch(Exception ex){
            ex.printStackTrace();
        }//close catch statement



        return customers;
    }//close getAllCustomers


    //Adds a new customer to the database
    public boolean addCustomer(Customer x){

        Customer temp = x;

        try{
            connection = DriverManager.getConnection(server, username, password);

            String prequery = "INSERT INTO Customers (FirstName, LastName, Address, Phone, City, State, Zip) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement query = connection.prepareStatement(prequery);
            query.setString(1, x.getFirstName());
            query.setString(2, x.getLastName());
            query.setString(3, x.getAddress());
            query.setString(4, x.getPhoneNumber());
            query.setString(5, x.getCity());
            query.setString(6, x.getState());
            query.setString(7, x.getZip());


            query.execute();

            connection.close();

        }//close try statement
        catch(Exception ex){
            ex.printStackTrace();
            return false;
        }//close catch

        return true;
    }//close addCustomer()

    //gets customer information based on their ID... will return single customer object
    public Customer getCustomerById(int id){
        Customer temp = new Customer();

        try {
            ResultSet results = null;
            connection = DriverManager.getConnection(server, username, password);
            String prequery = "SELECT * FROM CUSTOMERS WHERE id=?";
            PreparedStatement query = connection.prepareStatement(prequery);
            query.setInt(1, id);

            results = query.executeQuery();

            while(results.next()){
                temp.setID(results.getInt("id"));
                temp.setFirstName(results.getString("FirstName"));
                temp.setLastName(results.getString("LastName"));
                temp.setAddress(results.getString("Address"));
                temp.setPhoneNumber(results.getString("Phone"));
                temp.setCity(results.getString("City"));
                temp.setState(results.getString("State"));
                temp.setZip(results.getString("Zip"));

            }//close while statement

            connection.close();
        }//close try statement
        catch(Exception ex){
            System.out.print(ex.getMessage());
        }//close catch

        return temp;

    }//close getCustomerById()

    //updates a customer based off of their ID
    public void updateCustomer(Customer x){
        try{
            connection = DriverManager.getConnection(server, username, password);
            String prequery = "UPDATE Customers SET FirstName = ?, LastName = ?, Address = ?, Phone = ?, City = ?, State = ?, Zip = ? WHERE id = ?";
            PreparedStatement query = connection.prepareStatement(prequery);
            query.setString(1, x.getFirstName());
            query.setString(2, x.getLastName());
            query.setString(3, x.getAddress());
            query.setString(4, x.getPhoneNumber());
            query.setString(5, x.getCity());
            query.setString(6, x.getState());
            query.setString(7, x.getZip());
            query.setInt(8, x.getID());

            query.execute();

            connection.close();

        }//close try statement
        catch(Exception ex){
            ex.printStackTrace();
        }//close catch exception


    }//close updateCustomer()

    //deletes a customer from the Customers table based on their ID
    public void deleteCustomer(Customer x){
        try{
            connection = DriverManager.getConnection(server, username, password);

            String prequery = "DELETE FROM Customers WEHRE id = ?";
            PreparedStatement query = connection.prepareStatement(prequery);
            query.setInt(1, x.getID());

            query.execute();

            connection.close();

        }//close try statement
        catch(Exception ex){
            ex.printStackTrace();
        }//close Exception


    }//close deleteCustomer






}//close CustomerDAO
