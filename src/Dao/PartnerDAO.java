package Dao;

import Partner.*;

import java.sql.*;
import java.util.*;


public class PartnerDAO {
	private String server = "jdbc:mysql://162.218.48.24:3306/Lakeshore";
    private String username = "LakeShore";
    private String password = "284028";
    
    private static Set<Partner> partners = new HashSet<Partner>();
    
    
    private Connection connection;
    
	public PartnerDAO(){
		
	}//close underloaded constructor
	
	public Set<Partner> getAllPartners(){
		ResultSet temp;
		
		try{
			connection = DriverManager.getConnection(server, username, password);
			
			String prequery = "SELECT * FROM Partners";
			PreparedStatement query = connection.prepareStatement(prequery);
			temp = query.executeQuery();
			
			while(temp.next()){
				partners.add(new Partner(temp.getInt("id"), temp.getString("FirstName"),
						temp.getString("LastName"), temp.getString("Address"), temp.getString("PhoneNumber"),
						temp.getString("City"), temp.getString("State"), temp.getString("Zip"), 
						temp.getString("Comapny")));
			}//close while loop
			
		}//close try statement
		catch(Exception ex){
			ex.printStackTrace();
			
		}//close catch exception
		return partners;
	}//closegetAllPartners

	public Partner addPartner(String firstName, String lastName, String address, String phone,
			String city, String state, String zip, String company){
		Partner temp =  new Partner(firstName, lastName, address, phone, city, state, zip, company);
		
		try{
			connection = DriverManager.getConnection(server, username, password);
			String prequery = "";
			PreparedStatement query = connection.prepareStatement(prequery);
			
			query.execute();
			
			connection.close();
		}//try
		catch(Exception ex){
			ex.printStackTrace();
		}//catch
		
		
		
		return temp;
	}//close addPartner()
	
}//close PartnerDAO class
