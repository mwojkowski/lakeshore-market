package Service;

import java.util.*;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.CacheControl;


import Service.Representation.*;
import Service.Workflow.*;

/**
 * Created by Matthew on 10/26/2016.
 */
@Path("/CustomerService/")
public abstract class CustomerResource implements CustomerService {
	
	
	@GET
	@Produces({"application/xml", "application/json"})
	@Path("/Customers/")
	public Set<CustomerRepresentation> getCustomers(){
		CustomerActivity custActivity = new CustomerActivity();
		return custActivity.getCustomers();
	}//close getCustomers()
			
	
	@GET
	@Produces({"application/xml", "application/json"})
	@Path("/customer/{customerID}")
	public CustomerRepresentation getCustomer(@PathParam("customerID") int id){
		
		CustomerActivity custActivity = new CustomerActivity();
		
		return custActivity.getCustomerById(id);
		
	}//close getCustomer()
	
	@POST
	@Produces({"appliation/xml", "application/json"})
	@Path("/customer/")
	public CustomerRepresentation addCustomer(@PathParam("FirstName") String firstName, @PathParam("LastName") String lastName),
	@PathParam("Address") String address, @PathParam("Phone") String phoneNumber, @PathParam("City") String city,
	@PathParam("State") String state, @PathParam("Zip") String zip){
		CustomerActivity custActivity = new CustomerActivity();
		
		
		return custActivity.createCustomer(firstName, lastName, address, phoneNumber, city, state, zip);
	}//close addCustomer
	
	
	
	


}//close CustomerResource
