package Service.Workflow;

import Customer.*;
import Service.Representation.*;

import java.util.*;


/**
 * Created by Matthew on 10/26/2016.
 */
public class CustomerActivity {

    private static CustomerManager cManager = new CustomerManager();
    
    public CustomerActivity(){
    
    }

    public Set<CustomerRepresentation> getCustomers(){
        Set<Customer> customers = new HashSet<Customer>();

        Set<CustomerRepresentation> customerRepresentations = new HashSet<CustomerRepresentation>();

        customers = cManager.getAllCustomers();
        Iterator<Customer> cIterator = customers.iterator();
        while(cIterator.hasNext()){
            Customer temp = (Customer)cIterator.next();
            CustomerRepresentation cusRepTemp = new CustomerRepresentation();
            cusRepTemp.setID(temp.getID());
            cusRepTemp.setFirstName(temp.getFirstName());
            cusRepTemp.setLastName(temp.getLastName());
            cusRepTemp.setAddress(temp.getAddress());
            cusRepTemp.setPhoneNumber(temp.getPhoneNumber());
            cusRepTemp.setCity(temp.getCity());
            cusRepTemp.setState(temp.getState());
            cusRepTemp.setZip(temp.getZip());

            customerRepresentations.add(cusRepTemp);
        }//close while loop

        return customerRepresentations;
    }//close getCustomers()

    public CustomerRepresentation getCustomerById(int id){
        Customer temp = cManager.getCustomerById(id);

        CustomerRepresentation cusRep = new CustomerRepresentation();
        cusRep.setID(temp.getID());
        cusRep.setFirstName(temp.getFirstName());
        cusRep.setLastName(temp.getLastName());
        cusRep.setAddress(temp.getAddress());
        cusRep.setPhoneNumber(temp.getPhoneNumber());
        cusRep.setCity(temp.getCity());
        cusRep.setState(temp.getState());
        cusRep.setZip(temp.getZip());

        return cusRep;

    }//close getCustomerById()

    public CustomerRepresentation createCustomer(String firstName, String lastName, String address, String phoneNumber,
                                                 String city, String state, String zip){
        Customer cust = cManager.addCustomer(new Customer(firstName, lastName, address, phoneNumber, city, state, zip));

        CustomerRepresentation custRep = new CustomerRepresentation();
        custRep.setID(cust.getID());
        custRep.setFirstName(cust.getFirstName());
        custRep.setLastName(cust.getLastName());
        custRep.setAddress(cust.getAddress());
        custRep.setPhoneNumber(cust.getPhoneNumber());
        custRep.setCity(cust.getCity());
        custRep.setState(cust.getState());
        custRep.setZip(cust.getZip());

        return custRep;
    }//close createCustomer

   //public CustomerRepresentation updateCustomer()




}//close class CustomerActivity
