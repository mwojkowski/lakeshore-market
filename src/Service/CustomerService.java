package Service;

import java.util.Set;
import javax.jws.WebService;

import Service.Representation.*;

/**
 * Created by Matthew on 10/26/2016.
 */
public interface CustomerService {

    public Set<CustomerRepresentation> getCustomers();
    public CustomerRepresentation getCustomer(int id);
    public CustomerRepresentation createCustomer(CustomerRequest cRequest);


}//close interface
