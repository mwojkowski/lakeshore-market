package Person;

/**
 * Created by mwojkowski1 on 9/28/2016.
 */
public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;
    private String city;
    private String state;
    private String zip;

    //no constructors... class will be extended

    public int getID(){ return id ; }

    public void setID(int id){
        this.id = id;
    }

    public String getFirstName(){   return firstName;   }

    public String getLastName(){    return lastName;    }

    public String getAddress(){     return address;     }

    public String getPhoneNumber(){ return phoneNumber; }

    public String getCity(){    return city;    }

    public String getState(){   return state;   }

    public String getZip(){ return zip; }

    public void setFirstName(String x){   this.firstName = x;    }

    public void setLastName(String x){  this.lastName = x;  }

    public void setAddress(String x){   this.address = x;   }

    public void setPhoneNumber(String x){   this.phoneNumber = x; }

    public void setCity(String x){  this.city = x;  }

    public void setState(String x){ this.state = x; }

    public void setZip(String x){   this.zip = x;    }





}
