package Order;
/**
 * Created by mwojkowski1 on 9/28/2016.
 */
public class Order {
    private int id;
    private int partnerID;
    private int customerID;
    private double total;
    private String status; //SUBMITTED, SHIPPED, DELIVERED, CANCELED will be enforced by

    public Order(int id) {
        this.id = id;
        partnerID = 0;
        customerID = 0;
        total = 0.0;
        status = "";

    }//close Order(id)


    public Order() {

    }//close Order;

    public int getPartnerID(){  return partnerID;   }

    public int getCustomerID(){ return customerID;  }

    public double getTotal(){    return total;   }

    public String getStatus(){
        return this.status;
    }

    public String setStatus(String x) { return this.status;}

}
