package Review;
/**
 * Created by mwojkowski1 on 9/28/2016.
 */
import java.util.Date;
public class Review {
    private int reviewID;
    private int itemID;
    private int customerID;
    private String reviewTitle;
    private Date reviewDate;
    private String reviewText;
    private int rating;

    //used when adding a new review, reviewID will
    public Review(int itemID, int customerID, String reviewTitle, Date reviewDate, String reviewText, int rating){
        this.itemID = itemID;
        this.customerID = customerID;
        this.reviewTitle = reviewTitle;
        this.reviewDate = reviewDate;
        this.reviewText = reviewText;
        this.rating = rating;


    }//Review constructor

    //used when retrieving from the database for relational purposes
    public Review(int reviewID, int itemID, int customerID, String reviewTitle, Date reviewDate, String reviewText, int rating){
        this.reviewID = reviewID;
        this.itemID = itemID;
        this.customerID = customerID;
        this.reviewTitle = reviewTitle;
        this.reviewDate = reviewDate;
        this.reviewText = reviewText;
        this.rating = rating;

    }//Review constructor

    public int getReviewID(){   return reviewID;    }

    public int getitemID(){ return itemID;  }

    public int getcustomerID(){ return customerID;   }

    public String getReviewTitle(){ return reviewTitle; }

    public Date getReviewDate(){  return reviewDate;    }

    public String getReviewText(){  return reviewText;  }

    public int getRating(){ return rating;  }






}//Review
